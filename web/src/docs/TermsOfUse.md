Last modified: Feb 9, 2021

Acceptance of the Terms of Use

Welcome to the website of Dasvandh Network ("Company", "we", or "us", or "DVN"). The following terms and conditions (together with any documents referred to in them) (collectively, these "Terms of Use") apply to your use of the Dasvandh Network website, (the "Website") including any content, functionality and services offered on or through the Website, whether as a guest or a registered user.

Please read the Terms of Use carefully before you start to use the site. By using the Website or by clicking to accept or agree to the Terms of Use when this option is made available to you, you accept and agree to be bound and abide by these Terms of Use incorporated here by reference. If you do not want to agree to these Terms of Use or the Privacy Policy, you must exit the Website.

Who we are

DVN is a non-profit organization based in Texas. DVN aims to inspire a new level of giving to Panthic and related charitable projects across North America by providing awareness, transparency and accessibility to these efforts.

What we do

DVN will host a wide range of charitable, non-profit organizations and projects. These organizations or projects may or may not have 501(c)(3) tax status. Donations will be held by DVN and distributed to organizations and projects on a regular basis as milestones are hit or in accordance with the respective agreements between DVN and organizations or projects.

Changes to the Terms of Use

We may revise and update these Terms of Use from time to time in our sole discretion. You are expected to check this page from time to time to take notice of any changes we made, as they are binding on you. Your continued use of the Website following the posting of revised Terms of Use means that you accept and agree to the changes.

Accessing the Website and Account Security

By using this Website, you represent and warrant that you are 13 years or older.

We reserve the right to withdraw or amend this Website, and any service or material we provide on the Website, in our sole discretion without notice. We will not be liable if for any reason all or any part of the Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts of the Website, or the entire Website, to users, including registered users.

You are responsible for making all arrangements necessary for you to have access to the Website.

You are responsible for ensuring that all persons who access the Website through your Internet connection are aware of these Terms of Use, and that they comply with them.

To access the Website or some of the resources it offers, you may be asked to provide certain registration details or other information. It is a condition of your use of the Website that all the information you provide on the Website is correct, current and complete.

If you choose, or you are provided with, a user name, password or any other piece of information as part of our security procedures, you must treat such information as confidential, and you must not disclose it to any third party. You agree to immediately notify the Company of any unauthorized use of your user name or password or any other breach of security. You also agree to ensure that you exit from your account at the end of each session. You should use particular caution when accessing your account from a public or shared computer so that others are not able to view or record your password or other personal information.

We have the right to disable any user identification code or password, whether chosen by you or provided by us, at any time in our sole discretion for any or no reason, including, if in our opinion, you have failed to comply with any provision of these Terms of Use.

DVN FUNDRAISER AND DONOR ACCOUNTS

DVN requires all visitors who wish to donate ("donors") or raise funds for a project ("fundraisers") to sign up for a donor or fundraiser account by completing all required fields on the respective registration web pages on the Site. As part of the registration process, you agree to provide: (a) information about yourself that is truthful, current, complete and accurate; and (b) update this information so as to maintain its accuracy.

In order to donate on DVN you must provide DVN with valid credit card information. By submitting such credit card information, you give DVN permission to charge all payments incurred through your account to the credit card you designate at the time of your registration. All payments will be charged at the time they are incurred. Your account will be personal to you and you may not sub-license, transfer, sell or assign your account to any third party without DVN's approval. Any attempt to do so will be void and may result in the cancellation of your account without refund and in additional charges based on unauthorized use. DVN further reserves the right to withhold and/or cancel donations to accounts prior to account cancellation or termination, for suspected violations of these Terms of Use, for suspected unlawful activity, at its sole and absolute discretion.

Additionally, DVN reserves the right to terminate accounts created by users who appropriate the name, e-mail address, or other personally identifiable information of another individual. DVN, also reserves the right to refuse to approve a user account or terminate an existing user account with or without cause or notice (other than any notice required by applicable law and not waived herein) for any reason at any time. DVN is not liable for any losses or damages incurred by you as a result of the termination of your account or refusal of the service.

DVN and third parties acting on DVN's behalf may send you e-mail notifications from time to time in order to inform you about important information regarding your use of the Site and DVN Service. You may opt out from receiving certain e-mails by selecting the "opt out" feature in your user profile. However, even if you opt out you understand and agree that you will still be held responsible for the information contained in these e-mails.

You are solely responsible for maintaining the confidentiality of your password, username and all uses of your fundraiser account regardless of whether you have authorized such uses. You shall not share your account with anyone else. You agree not to use the account, username or password of anyone else. You shall not disclose your password to anyone else. Multiple accounts are prohibited. You agree to have only one account. You agree to exit from your account at the end of each session and to take all reasonable precautions to prevent others from using your account. You agree to notify DVN immediately of any unauthorized use of your account.

MAKING A DONATION THROUGH DVN

DVN provides online fundraising tools to non-profit organizations, individuals raising money on behalf of non-profit organizations, and individuals who wish to raise money for projects unaffiliated with a non-profit organization. Because DVN allows individuals to raise money for a large number and variety of projects, DVN has not researched every project on the site and makes no representations and does not warrant that donations to any particular project are tax-deductible.

Additionally, DVN makes no representations or warranties regarding the accuracy of any information contained in Users' profile and project pages. Users use the Site entirely at their own risk. DVN does not warrant that the information listed in a User's profile page is an accurate description of the User or a real person. DVN does not warrant that the information listed in a User's project page is an accurate description of the project, the User's intended use of donations or that the project exists. DVN does not warrant that a User who receives donations for a project will use the funds in a manner specified in their project description. If you have any reservations regarding the legitimacy of a particular organization/project, fundraiser or fundraising event, DO NOT DONATE.

FEES, INTEREST EARNED AND TAXES

The minimum donation amount is $10.

There is a 3.5% processing fee for all donations made by credit/debit card and $0.50 fee for all ACH donations—these fees are used to cover payment processing fees we pay to 3rd party providers. There are no processing fees for donations made by check.

For organizations/projects: once you receive donations from DVN, you are legally responsible for the funds (DVN no longer has liability). Furthermore, any use of these funds is strictly in your control. You must notify DVN within 30 days of receipt of funds to report an issue with the amount received. After 30 days, DVN is no longer legally liable but may make adjustments if needed. Any inquiries by donors or other parties will be referred to your organization/project.

All donations made through DVN are final and non-refundable. If a donor-initiated chargeback occurs, DVN will do what it can to resolve the situation, and if necessary, reinstate these charges. However, if a chargeback occurs, DVN will have to reduce funds scheduled to be sent to an organization/project. If funds have already been released to an organization/project, then the organization/project will have to return these funds back to DVN.

By donating through Dasvandh Network, your donation history and records will be accessible by Dasvandh Network and the organization to which you donated. Please visit our Privacy Policy for more information.

Intellectual Property Rights

The Website and its entire contents, features and functionality (including but not limited to all information, software, text, displays, images, video and audio, and the design, selection and arrangement thereof), are owned by the Company, its licensors or other providers of such material and are protected by United States and international copyright, trademark, patent, trade secret and other intellectual property or proprietary rights laws.

You are permitted to use the Website for your personal use only. You must not copy, modify, create derivative works of, publicly display, publicly perform, republish, download, store or transmit any of the material on our site, except to:

Store copies of such materials temporarily in RAM;
Store files that are automatically cached by your Web browser for display enhancement purposes; and
Print a reasonable number of pages of the Website for a permitted use
You must not:

Modify copies of any materials from this site;
Use any illustrations, photographs, video or audio sequences or any graphics separately from the accompanying text; or
Delete or alter any copyright, trademark or other proprietary rights notices from copies of materials from this site
You must not reproduce, sell or exploit for any commercial purposes any part of the Website, access to the Website or use of the Website or any services or materials available through the Website.

If you print, copy, modify, download or otherwise use any part of the Website in breach of the Terms of Use, your right to use the Website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. No right, title or interest in or to the Website or any content on the site is transferred to you, and all rights not expressly granted are reserved by the Company. Any use of the Website not expressly permitted by these Terms of Use is a breach of these Terms of Use and may violate copyright, trademark and other laws.

Company Trademarks

The Company name, the terms, the Company logo and all related names, logos, product and service names, designs and slogans are trademarks of the Company or its affiliates or licensors. You must not use such marks without the prior written permission of the Company. All other names, brands and marks are used for identification purposes only and are the trademarks of their respective owners.

Prohibited Uses

You may use the Website only for lawful purposes and in accordance with these Terms of Use. You agree not to use the Website:

In any way that violates any applicable federal, state, local and international law or regulation (including, without limitation, any laws regarding the export of data or software to and from the US or other countries).
For the purpose of exploiting, harming or attempting to exploit or harm minors in any way by exposing them to inappropriate content, asking for personally identifiable information or otherwise.
To send, knowingly receive, upload, download, use or re-use any material which does not comply with the Content Standards set out below in these Terms.
To transmit, or procure the sending of, any advertising or promotional material, including any "junk mail", "chain letter" or "spam" or any other similar solicitation.
To impersonate or attempt to impersonate the Company or a Company employee, another user, or person or entity (including, without limitation, the use of e-mail addresses associated with any of the foregoing).
To engage in any other conduct that restricts or inhibits anyone's use or enjoyment of the Website, or which, as determined by us, may harm the Company or users of the Website or expose them to liability.
Additionally, you agree not to:

Use the Website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party's use of the Website, including their ability to engage in real time activities through the Website.
Use any robot, spider or other automatic device, process or means to access the Website for any purpose, including to monitor or copy any of the material on the Website.
Use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent.
Use any device, software or routine that interferes with the proper working of the Website.
Introduce any viruses, trojan horses, worms, logic bombs or other material which is malicious or technologically harmful.
Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or database connected to the Website.
Attack the Website via a denial-of-service attack or a distributed denial-of-service attack.
Otherwise attempt to interfere with the proper working of the Website.
TRANSACTIONS BETWEEN PARTIES

DVN serves as an intermediary between donors and beneficiaries. As needed and agreed upon, DVN remits all donated funds (less any processing fee) to the organization/project's beneficiary in the form of an ACH bank transfer. If a DVN listed organization/project is found to be in violation of the terms of this User Agreement, DVN reserves the right to terminate the organization/project and user account. Finally, DVN can cap the number of projects an organization can list on DVN.

DVN does not warrant that funds will be used for any particular purpose and is not responsible for any misuse of the funds by the beneficiary. After DVN transfers funds to the beneficiary, all further dealings are solely between you and such organizations and individuals. By using this site you understand and agree that DVN shall not be responsible for any losses or damages incurred as a result of the organization/project. In the event of a dispute between users (including but not limited to fundraisers, donors, beneficiaries, and third parties), you agree to release DVN, its employees, agents, affiliates, directors, officers, representatives, subcontractors, advisors and volunteers or anyone else who has participated in the creation development or delivery of this Site (collectively "DVN and its Affiliates") from all claims, damages and demands that may or may not be known, suspected or related to such disputes about our service. While DVN shall not be held liable for the actions of a fundraiser, DVN nevertheless reserves the right to terminate an organization/project at any time and for any reason. DVN also reserves the right to remove an organization/project and/or User from the Site at any time and for any reason.

User Contributions

The Website may contain message boards, chat rooms, personal web pages or profiles, forums, bulletin boards and other interactive features (collectively, "Interactive Services") that allow you to post, submit, publish, display or transmit to other users (hereinafter, "post") content or materials (collectively, "User Contributions") on or through the Website.

All User Contributions must comply with the Content Standards set out in these Terms.

Any User Contribution you post to the site will be considered non-confidential and non-proprietary, and we have the right to use, copy, distribute and disclose to third parties any such material for any purpose.

You represent and warrant that you own or control all rights in and to the User Contributions and have the right to grant the Company and its affiliates the license granted above.

You represent and warrant that all of your User Contributions do and will comply with these Terms of Use, and you agree to defend, indemnify and hold harmless the Company and its affiliates and licensors for any breach of that representation and warranty.

You understand and acknowledge that you are responsible for any User Contributions you submit or contribute, and you, not the Company, have fully responsibility for such content, including its legality, reliability, accuracy and appropriateness.

Any content and/or opinions uploaded, expressed or submitted to the Website, and all articles and responses to questions and other content, other than the content provided by the Company, are solely the opinions and the responsibility of the person or entity submitting them and do not necessarily reflect the opinion of the Company. We are not responsible, or liable to any third party, for the content or accuracy of any materials posted by you or any other user of the Website.

Monitoring and Enforcement; Termination

We have the right to:

Remove or refuse to post any User Contributions for any or no reason in our sole discretion.
Take any action with respect to any User Contribution that we deem necessary or appropriate in our sole discretion if we believe that such User Contribution violates the Terms of Use, including the Content Standards, infringes any intellectual property right or other right, threatens the personal safety of users of the Website and the public or could create liability for the Company.
Disclose your identity to any third party who claims that material posted by you violates their rights, including their intellectual property rights or their right to privacy.
Take appropriate legal action, including without limitation, referral to law enforcement, for any illegal or unauthorized use of the Website.
Terminate your access to all or part of the Website for any or no reason, including without limitation, any violation of these Terms of Use.
Without limiting the foregoing, we have the right to fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity of anyone posting any materials on or through the Website. YOU WAIVE AND HOLD HARMLESS THE COMPANY FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY THE COMPANY DURING OR AS A RESULT OF ITS INVESTIGATIONS AND FROM ANY ACTIONS TAKEN AS A CONSEQUENCE OF INVESTIGATIONS BY EITHER THE COMPANY OR LAW ENFORCEMENT AUTHORITIES.

However, we cannot review all material before it is posted on the Website, and cannot ensure prompt removal of objectionable material after it has been posted. Accordingly, we assume no liability for any action or inaction regarding transmissions, communications or content provided by any user or third party. We have no liability or responsibility to anyone for performance or non-performance of the activities described in this paragraph.

Content Standards

These content standards apply to any and all User Contributions and Interactive Services. User Contributions must in their entirety comply with all applicable federal, state, local and international laws and regulations. Without limiting the foregoing, User Contributions must not:

Contain any material, which is defamatory, obscene, indecent, abusive, offensive, harassing, violent, hateful, inflammatory or otherwise objectionable.
Promote sexually explicit or pornographic material, violence, or discrimination based on race, sex, religion, nationality, disability, sexual orientation or age.
Infringe any patent, trademark, trade secret, copyright or other intellectual property rights of any other person.
Violate the legal rights (including the rights of publicity and privacy) of others or contain any material that could give rise to any civil or criminal liability under applicable laws or regulations or that otherwise may be in conflict with these Terms of Use and our Privacy Policy
Be likely to deceive any person.
Promote any illegal activity, or advocate, promote or assist any unlawful act.
Cause annoyance, inconvenience or needless anxiety or be likely to upset, embarrass, alarm or annoy any other person.
Be used to impersonate any person, or to misrepresent your identity or affiliation with any person or organization.
Involve profit-making activities or sales, such as contests, sweepstakes and other sales promotions, barter or advertising.
Give the impression that they emanate from us or any other person or entity, if this is not the case.
Copyright Infringement

We take claims of copyright infringement seriously. We will respond to notices of alleged copyright infringement that comply with applicable law. If you believe any materials accessible on or from this site (the "Website") infringe your copyright, you may request removal of those materials (or access thereto) from the Website by submitting written notification to us at the address designated below. In accordance with the Online Copyright Infringement Liability Limitation Act of the Digital Millennium Copyright Act (17 U.S.C. § 512) ("DMCA"), the written notice (the "DMCA Notice") must include substantially the following:

Your physical or electronic signature.
Identification of the copyrighted work you believe to have been infringed or, if the claim involves multiple works on the Website, a representative list of such works.
Identification of the material you believe to be infringing in a sufficiently precise manner to allow us to locate that material.
Adequate information by which we can contact you (including your name, postal address, telephone number and, if available, e-mail address).
A statement that you have a good faith belief that use of the copyrighted material is not authorized by the copyright owner, its agent or the law.
A statement that the information in the written notice is accurate.
A statement, under penalty of perjury, that you are authorized to act on behalf of the copyright owner.
Our designated Address to receive DMCA Notices is:

Dasvandh Network
6002 Camp Bullis Rd
San Antonio, TX 78257
248-906-8039

If you fail to comply with all of the requirements of Section 512(c)(3) of the DMCA, your DMCA Notice may not be effective.

Please be aware that if you knowingly materially misrepresent that material or activity on the Website is infringing your copyright, you may be held liable for damages (including costs and attorneys' fees) under Section 512(f) of the DMCA.

It is our policy in appropriate circumstances to disable and/or terminate the accounts of users who are repeat infringers.

Reliance on Information Posted

The information presented on or through the Website is made available solely for general information purposes. We do not warrant the accuracy, completeness or usefulness of this information. Any reliance you place on such information is strictly at your own risk. We disclaim all liability and responsibility arising from any reliance placed on such materials by you or any other visitor to the Website, or by anyone who may be informed of any of its contents.

Changes to the Website

We may update the Website from time to time, but its content is not necessarily complete or up-to-date. Any of the material on the Website may be out of date at any given time, and we are under no obligation to update such material. We may change the Website at any time with or without notice. We may suspend access to the Website, or close it indefinitely.

Information About You and Your Visits to the Website

All information we collect on this Website is subject to our Privacy Policy. By using the Website, you consent to all actions taken by us with respect to your information in compliance with the Privacy Policy. You represent and warrant that all data provided by you is accurate.

Linking to the Website

You may link to our homepage, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.

You must not establish a link from any website that is not owned by you.

The Website must not be framed on any other site, nor may you create a link to any part of the Website other than the homepage.

The website from which you are linking must comply in all respects with the Content Standards set out in these Terms of Use.

You agree to cooperate with us in causing any unauthorized framing or linking immediately to cease. We reserve the right to withdraw linking permission without notice.

Links from the Website

If the Website contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links contained in advertisements, including banner advertisements and sponsored links. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. If you decide to access any of the third party websites linked to this Website, you do so entirely at your own risk and subject to the terms and conditions of use for such websites.

Geographic Restrictions

The owner of the Website is based in the state of Illinois in the United States. We provide this Website for use only by persons located in the United States. We make no claims that the Website or any of its content is accessible or appropriate outside of the United States. Access to the Website may not be legal by certain persons or in certain countries. If you access the Website from outside the United States, you do so on your own initiative and are responsible for compliance with local laws.

Disclaimer of Warranties

You understand that we cannot and do not guarantee or warrant that files available for downloading from the internet or the Website will be free of viruses or other destructive code. You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for anti-virus protection and accuracy of data input and output, and for maintaining a means external to our site for any reconstruction of any lost data. WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY WEBSITE LINKED TO IT.

YOUR USE OF THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY PERSON ASSOCIATED WITH THE COMPANY MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF THE WEBSITE. WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR ANYONE ASSOCIATED WITH THE COMPANY REPRESENTS OR WARRANTS THAT THE WEBSITE, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.

THE COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR PARTICULAR PURPOSE.

THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.

Limitation on Liability

IN NO EVENT WILL THE COMPANY, ITS AFFILIATES OR THEIR LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE, ANY WEBSITES LINKED TO IT, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.

THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.

Indemnification

You agree to defend, indemnify and hold harmless the Company, its affiliates and licensors and their respective officers, directors, employees, contractors, agents, licensors and suppliers from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses or fees (including reasonable attorneys' fees) resulting from your violation of these Terms of Use or your use of the Website, including, without limitation, any use of the Website's content, services and products other than as expressly authorized in these Terms of Use or your use of any information obtained from the Website.

Governing Law and Jurisdiction

These Terms of Use and any dispute or claim arising out of, or related to, them, their subject matter or their formation (in each case, including non-contractual disputes or claims) shall be governed by and construed in accordance with the internal laws of the State of Illinois without giving effect to any choice or conflict of law provision or rule (whether of the State of Illinois or any other jurisdiction).

Any legal suit, action or proceeding arising out of, or related to, these Terms of Use or the Website shall be instituted exclusively in the federal courts of the United States or the courts of the State of Illinois, in each case located in the City of Chicago and County of Cook although we retain the right to bring any suit, action or proceeding against you for breach of these Terms of Use in your country of residence or any other relevant country. You agree to waive any and all objections to the exercise of jurisdiction over you by such courts and to venue in such courts.

Limitation on Time to File Claims

ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS OF USE OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.

Waiver and Severability

No waiver of these Terms of Use by the Company shall be deemed a further or continuing waiver of such term or condition or any other term or condition, and any failure of the Company to assert a right or provision under these Terms of Use shall not constitute a waiver of such right or provision.

If any provision of these Terms of Use is held by a court of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms of Use will continue in full force and effect.

Entire Agreement

The Terms of Use and our Privacy Policy constitute the sole and entire agreement between you and DVN with respect to the Website and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral,with respect to the Website.

Your Comments and Concerns

This website is operated by Dasvandh Network at [6002 Camp Bullis Rd, San Antonio, TX 78257].

All other feedback, comments, requests for technical support and other communications relating to the Website should be directed to: info@dvnetwork.org.

Thank you for visiting the Website.
