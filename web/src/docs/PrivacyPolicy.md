Last modified: Feb 9, 2021

Introduction

Dasvandh Network (“DVN" or "Company" or "we" or “DVN") respects your privacy and is committed to protecting it through its compliance with this policy.

This policy describes the types of information we may collect from you or that you may provide when you visit the website http://www.dvnetwork.org (the "Website") and our practices for collecting, using, maintaining, protecting and disclosing that information.

This policy applies to information we collect:

On the Website.
In e-mail, text and other electronic messages between you and the Website.
It does not apply to information collected by:

us offline or through any other means, including on any other website operated by the Company or any third party; or
any third party (including our affiliates and subsidiaries), including through any application or content (including advertising) that may link to or be accessible from the Website
Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use the Website. By accessing or using the Website, you agree to this privacy policy. This policy may change from time to time. Your continued use of the Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.

Children Under the Age of 13

The Website is not intended for children under 13 years of age. No one under age 13 may provide any information to the Website. We do not knowingly collect personal information from children under 13. If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at 6002 Camp Bullis Rd, San Antonio, TX 78257 or at info@dvnetwork.org.

Information We Collect About You and How We Collect It

We collect several types of information from and about users of the Website, including information:

by which you may be personally identified, such as name, postal address, e-mail address or telephone number ("personal information");
that is about you but individually does not identify you; and/or
about your Internet connection, the equipment you use to access our Website and usage details.
We collect this information:

Directly from you when you provide it to us.
Automatically as you navigate through the site. Information collected automatically may include usage details, IP addresses and information collected through cookies.
Information You Provide to Us

The information we collect on or through the Website may include:

Information that you provide by filling in forms on our Website. This includes information provided at the time of registering to use the Website, posting material, or requesting further services.
When making a donation by credit card, you will have to enter your credit card information. This information WILL NOT be stored on the Dasvandh Network site. Our payment processing partners, Braintree Payment Solutions and Forte Payment Systems, provide solutions to prevent credit card and banking data from being compromised (DVN Site Certificate). All transactions through Dasvandh Network are made using Secure Sockets Layer (SSL) to keep information secure and protect against loss or misuse.
We may also ask you for information when you report a problem with the Website. If you donated anonymously, your name and e-mail address will not be revealed to other organizations/projects. DVN will keep a record of donations but won't reveal this information.
Records and copies of your correspondence (including e-mail addresses and telephone numbers), if you contact us.
Details of transactions you carry out through the Website and of the fulfilment of your orders.
You also may provide information to be published or displayed (hereinafter, "posted") on public areas of the Website, or transmitted to other users of the Website or third parties (collectively, "User Contributions"). Your User Contributions are posted on and transmitted to others at your own risk. Although you may set certain privacy settings for such information by logging into your account profile, please be aware that no security measures are perfect or impenetrable. Additionally, we cannot control the actions of other users of the Website with whom you may choose to share your User Contributions. Therefore, we cannot and do not guarantee that your User Contributions will not be viewed by unauthorized persons.

Usage Details, IP Addresses

As you navigate through and interact with the Website, we may automatically collect certain information about your equipment, browsing actions and patterns, including:

Details of your visits to the Website, including traffic data, location data, and other communication data and the resources that you access and use on the Website.
Information about your computer and Internet connection, including your IP address, operating system and browser type.
The information we collect automatically is statistical data, and does not identify any individual. It helps us to improve the Website and to deliver a better and more personalized service by enabling us to:

Estimate our audience size and usage patterns.
Store information about your preferences, allowing us to customize the Website according to your individual interests.
Speed up your searches.
Recognize you when you return to the Website.
The technologies we use for this automatic data collection may include:

Cookies (or browser cookies). A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Website.
Flash Cookies. Certain features of the Website may use local stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, from and on the Website. Flash cookies are not managed by the same browser settings as are used for browser cookies.
Third-parties and Use of Cookies

DVN respects your privacy and will never sell, trade, or rent your personal information to outside 3rd parties without consent. Your donation information will be available to the projects and organizations that you choose to donate to.

Our site uses cookies to keep track of your session ID. We may use cookies to deliver content specific to your interests. We may also track the pages you request, the newsletters to which you subscribe and the advertisements on which you click.

Some content and/or advertisements on the Website may be served by third-parties. These third parties may use cookies to collect information about our users. This may include information about users' behaviour on this and other websites to serve them interested-based (behavioural) advertising. We do not control these third parties' tracking technologies or how they may be used. If you have any questions about this, then you should contact the responsible advertiser directly.

DVN will have other organizations listed on its site and links to these organizations. We are not responsible for the privacy policies of these outside organizations.

How We Use Your Information

We use information that we collect about you or that you provide to us, including any personal information:

To present the Website and its contents to you.
To provide you with information, products or services that you request from us.
To fulfil any other purpose for which you provide it.
To carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.
To notify you about changes to the Website or any products or services we offer or provide though it.
In any other way we may describe when you provide the information.
For any other purpose with your consent.
We may also use your information to contact you about goods, services, and updates that may be of interest to you. If you do not want us to use your information in this way, please check the relevant box located on the form on which we collect your data and adjust your user preferences in your account profile.

Disclosure of Your Information

We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.

We may disclose personal information that we collect or you provide as described in this privacy policy:

To our subsidiaries and affiliates.
To contractors, service providers and other third parties we use to support our business and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.
To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of DVN's assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal information held by DVN about the Website users is among the assets transferred.
To fulfil the purpose for which you provide it.
For any other purpose disclosed by us when you provide the information.
With your consent.
We may also disclose your personal information:

To comply with any court order, law or legal process, including responding to any government or regulatory request.
To enforce or apply our terms of use and other agreements, including for billing and collection purposes.
If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of DVN, our customers or others.
Choices About How We Use and Disclose Your Information

We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you with the following control over your information:

Tracking Technologies and Advertising. You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe's Website. If you disable or refuse cookies, please note that some parts of this site may then be inaccessible or not function properly.

We do not control third parties' collection or use of your information to serve interest-based advertising. However these third parties may provide you with ways to choose not to have your information collected or used in this way. You can opt out of receiving targeted ads from members of the Network Advertising Initiative ("NAI") on the NAI's Website.
Accessing and Correcting Your Information

You can review and change your personal information by logging into the website and visiting your account profile page.

You may also send us an e-mail at info@dvnetwork.org to request access to, correct or delete any personal information that you have provided to us. We cannot delete your personal information. We may not accommodate a request to change information if we believe the change would violate any law or legal requirement or cause the information to be incorrect.

If you delete your User Contributions from the Website, copies of your User Contributions may remain viewable in cached and archived pages, or might have been copied or stored by other Website users. Proper access and use of information provided on the Website, including User Contributions, is governed by our terms of use.

Your California Privacy Rights

California Civil Code Section § 1798.83 permits users of our Website that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an e-mail to info@dvnetwork.org or write us at: [6002 Camp Bullis Rd, San Antonio, TX 78257]

Data Security

We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration and disclosure. All information you provide to us is stored on our secure servers behind firewalls. Any payment transactions will be encrypted.

The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a password for access to certain parts of our Website, you are responsible for keeping this password confidential. We ask you not to share your password with anyone.

Unfortunately, the transmission of information via the Internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Website. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Website.

Changes to Our Privacy Policy

It is our policy to post any changes we make to our privacy policy on this page. If we make material changes to how we treat our users' personal information, we may notify you by e-mail to the e-mail address specified in your account and/or through a notice on the Website home page. The date the privacy policy was last revised is identified at the top of the page. You are responsible for ensuring we have an up-to-date active and deliverable e-mail address for you, and for periodically visiting our website and this privacy policy to check for any changes.

Contact Information

To ask questions or comment about this privacy policy and our privacy practices, contact us at: info@dvnetwork.org.
