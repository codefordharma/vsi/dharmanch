/** @format */

const { USER_PERMISSION } = require("./schemaTypes");

function validateUserPermissions(values) {
  if (values.length < 1 && values.length > 2) {
    return false;
  }

  for (let i = 0; i < values.length; i += 1) {
    const value = values[i];

    const index = USER_PERMISSION.findIndex((v) => v === value);

    if (index < 0) {
      return false;
    }
  }

  return true;
}

module.exports = {
  validateUserPermissions,
};
