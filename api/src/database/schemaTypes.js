/** @format */

const USER_PERMISSION = ["USER", "ADMIN"];

module.exports = {
  USER_PERMISSION,
};
