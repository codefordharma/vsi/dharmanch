/** @format */

const mongoose = require("mongoose");

const { /* NODE_ENV, */ MONGODB_URI } = process.env;

// NOTE: disable autoIndex for production server so that it won't recreate
// all the indexes again and again, as warning given in mongoose website.

mongoose.connect(MONGODB_URI, {
  autoIndex: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

module.exports = mongoose;
