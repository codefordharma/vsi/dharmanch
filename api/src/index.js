/** @format */
require("./env");

const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const express = require("express");
const jwt = require("jsonwebtoken");
const util = require("util");
const multer = require("multer");
const { ApolloServer } = require("apollo-server-express");
const cors = require("cors");

const { resolvers, typeDefs } = require("./entities");
const { corsOptions } = require("./config");

const UploadFile = require("./entities/files/upload");
const UploadPhoto = require("./entities/photos/upload");
const UploadVideo = require("./entities/videos/upload");

const jwtVerifyPromise = util.promisify(jwt.verify);

const { AUTH_SECRET, PORT } = process.env;

const app = express();

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(async (req, res, next) => {
  const { authorization } = req.headers;

  if ((authorization || "").length > 10) {
    try {
      const obj = await jwtVerifyPromise(authorization, AUTH_SECRET);

      const { _id: userId } = obj;

      req.userId = userId;
    } catch (error) {
      console.error(error);
    }
  }

  next();
});

const upload = multer({
  storage: multer.memoryStorage(),
  limits: {
    // Maximum file size is 512KB
    fileSize: 512 * 1024,
  },
});

app.post("/upload-photo", upload.any(), UploadPhoto);
app.post("/upload-video", upload.any(), UploadVideo);
app.post("/upload-file", upload.any(), UploadFile);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  cors,
  context: (req) => ({
    ...req,
  }),
});

server.applyMiddleware({ app });

app.listen({ port: PORT }, () => {
  const path = `http://localhost:${PORT}${server.graphqlPath}`;

  console.log(`Server ready at ${path}`);
});
