/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type User {
    id: ID
    name: String
    email: String
    permissions: [PERMISSION!]
    avatar: String

    notificationEnabled: Boolean
    emailVerified: Boolean

    passwordResetToken: String
    passwordResetTokenExpiry: Float

    token: String
  }

  input UserCreateInput {
    name: String!
    email: String!
    password: String!
  }

  input UserUpdateInput {
    name: String!
    email: String!
    password: String!
  }

  input SignUpInput {
    name: String!
    email: String!
    password: String!
  }

  input SignInInput {
    email: String!
    password: String!
  }

  input PasswordResetInput {
    id: ID!
    password: String!
    password2: String!
  }

  extend type Query {
    currentUser: User
    users: [User]
    user(id: ID!): User
  }

  extend type Mutation {
    createUser(data: UserCreateInput!): User!
    updateUser(data: UserUpdateInput!): User!
    deleteUser: Boolean

    signup(data: SignUpInput!): User!
    signin(data: SignInInput!): User!
    signout: Boolean

    passwordReset(data: PasswordResetInput!): Boolean
  }
`;

module.exports = typeDef;
