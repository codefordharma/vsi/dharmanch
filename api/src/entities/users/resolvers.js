/** @format */
const { AUTH_SECRET } = process.env;

const crypto = require("crypto");
const jsonwebtoken = require("jsonwebtoken");
const { ObjectId } = require("mongoose").Types;

const UserModel = require("./model");

const resolvers = {
  Query: {
    currentUser: async (parent, args, context, info) => {
      const { userId } = context.req;

      const user = await UserModel.findById(ObjectId(userId));

      return user;
    },
    user: async (parent, args, context, info) => {
      const { id } = args;

      const user = await UserModel.findById(ObjectId(id));

      return user;
    },
    users: async (parent, args, context, info) => {
      const users = await UserModel.find();

      return users;
    },
  },
  Mutation: {
    signup: async (parent, args, context, info) => {
      const { email, password, name } = args.data;
      // const { host } = context.req.headers;

      const emailLowerCase = email.toLowerCase();
      const passwordHashed = crypto
        .createHash("sha256")
        .update(password)
        .digest("hex");

      const users = await UserModel.find({
        email,
      });

      if (users.length > 0) {
        throw new Error("Email Id Already Exist");
      }

      // const encryptedToken = crypto
      //   .createHash("sha256")
      //   .update(emailLowerCase + new Date().toString())
      //   .digest("hex");

      const newUser = new UserModel({
        name,
        email: emailLowerCase,
        password: passwordHashed,
        // emailVerifiedToken: encryptedToken,
        // emailVerifiedTokenExpiry: Date.now() + 86400000, // 1 day
      });

      const user = await newUser.save();
      const userObj = user.toObject();
      // const userId = user._id.toString();
      delete userObj.password;

      // JWT token
      const token = jsonwebtoken.sign(userObj, AUTH_SECRET);

      userObj.id = user._id;
      userObj.token = token;

      return userObj;
    },
    signin: async (parent, args, context, info) => {
      const { email, password } = args.data;
      // const { host } = context.req.headers;

      const emailLowerCase = email.toLowerCase();
      const passwordHashed = crypto
        .createHash("sha256")
        .update(password)
        .digest("hex");

      const users = await UserModel.find({
        email: emailLowerCase,
        password: passwordHashed,
      });

      if (!users.length) {
        throw new Error("Email & Password does not match.");
      }

      const [user] = users;
      const userObj = user.toObject();
      delete userObj.password;

      // JWT token
      const token = jsonwebtoken.sign(userObj, AUTH_SECRET);

      userObj.id = user._id;
      userObj.token = token;

      return userObj;
    },
    signout: async (parent, args, context, info) => {
      return true;
    },
    passwordReset: async (parent, args, context, info) => {
      const { id, password, password2 } = args.data;

      if (
        (password.length < 8 && password2.length < 8) ||
        password !== password2
      ) {
        throw new Error("Invalid Password");
      }

      const passwordHashed = crypto
        .createHash("sha256")
        .update(password)
        .digest("hex");

      try {
        await UserModel.findByIdAndUpdate(id, {
          password: passwordHashed,
        });

        return true;
      } catch (error) {
        return false;
      }
    },
  },
};

module.exports = resolvers;
