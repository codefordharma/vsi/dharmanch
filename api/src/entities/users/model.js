/** @format */

const isEmail = require("validator/lib/isEmail");

const mongoose = require("../../database/init");
const { validateUserPermissions } = require("../../database/schemaValidate");

const { Schema } = mongoose;

/**
 * User Permissions are as follows:
 * 'USER' => for normal user
 * 'ADMIN' => for admin panel access
 * 'MANAGER' => for general purpose access, not implementing it now.
 */

const UserSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      validate: [isEmail, "No valid email address provided."],
    },
    password: {
      type: String,
      required: true,
    },
    passwordResetToken: {
      type: String,
    },
    passwordResetTokenExpiry: {
      type: String,
    },

    permissions: {
      type: [String],
      default: ["USER"],
      required: true,
      // NOTE: make improvement if needed.
      validate: [validateUserPermissions, `Invalid permissions value`],
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  },
);

UserSchema.index(
  {
    email: 1,
  },
  {
    unique: true,
  },
);

const UserModel = mongoose.model("user", UserSchema, "users");

module.exports = UserModel;
