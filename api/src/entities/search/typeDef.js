/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  enum SearchType {
    TEMPLE
    DEITY
    ARTICLE
  }

  type Search {
    temples: [Temple]
    deities: [Deity]
    # articles: [Article]
  }

  input SearchWhereInput {
    limit: Int!
    page: Int!

    search: String!
    type: SearchType!
  }

  extend type Query {
    search(where: SearchWhereInput!): Search
  }
`;

module.exports = typeDef;
