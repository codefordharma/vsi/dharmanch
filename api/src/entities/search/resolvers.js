/** @format */

// const { ObjectId } = require("mongoose").Types;

const TempleModel = require("../temples/model");
const ArticleModel = require("../articles/model");
const DeityModel = require("../deities/model");

const resolvers = {
  Query: {
    search: async (parent, args, context, info) => {
      const { search, type, limit = 25, page = 1 } = args.where;

      if (page < 1) {
        throw new Error(`Page number can't be less then 1.`);
      }

      let conditions = {};
      let temples = [];
      let deities = [];
      let articles = [];

      if (typeof search !== "undefined") {
        conditions = {
          ...conditions,
          name: {
            $regex: search,
            $options: "i",
          },
        };
      }

      if (type === "TEMPLE") {
        temples = await TempleModel.find(conditions)
          .limit(limit)
          .skip((page - 1) * limit)
          .sort({ createdAt: "desc" });
      }

      if (type === "DEITY") {
        deities = await DeityModel.find(conditions)
          .limit(limit)
          .skip((page - 1) * limit)
          .sort({ createdAt: "desc" });
      }

      if (type === "ARTICLE") {
        articles = await ArticleModel.find(conditions)
          .limit(limit)
          .skip((page - 1) * limit)
          .sort({ createdAt: "desc" });
      }

      return {
        temples,
        deities,
        articles,
      };
    },
  },
  // Mutation: {},
};

module.exports = resolvers;
