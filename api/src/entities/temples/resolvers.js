/** @format */

// const { ObjectId } = require("mongoose").Types;
const fetch = require("node-fetch");

const TempleModel = require("./model");

const { GOOGLE_MAP_API_KEY } = process.env;

const resolvers = {
  Query: {
    temple: async (parent, args, context, info) => {
      const { id } = args.where;

      return TempleModel.findById(id);
    },
    templeCount: async (parent, args, context, info) => {
      const { isPublished } = args.where;
      let conditions = {};

      if (typeof isPublished !== "undefined") {
        conditions = {
          ...conditions,
          isPublished,
        };
      }

      return TempleModel.countDocuments(conditions);
    },
    temples: async (parent, args, context, info) => {
      // NOTE: we will give result step wise, with only 10 temples at
      // a time & as the number of 'page' increases, will give
      // next 10 temples
      const {
        id,
        ids,
        placeId,

        limit = 10,
        page = 1,
        country,
        state,
        district,
        street,
        pincode,

        isPublished,
      } = args.where;

      if (page < 1) {
        throw new Error(`Page count can't be less then 1.`);
      }

      let conditions = {};

      if (typeof id !== "undefined") {
        conditions = { ...conditions, _id: id };
      }

      if (typeof ids !== "undefined") {
        conditions = { ...conditions, _id: { $in: ids } };
      }

      if (typeof placeId !== "undefined") {
        conditions = { ...conditions, placeId };
      }

      if (typeof country !== "undefined") {
        conditions = { ...conditions, country };
      }

      if (typeof state !== "undefined") {
        conditions = { ...conditions, state };
      }

      if (typeof district !== "undefined") {
        conditions = { ...conditions, district };
      }

      if (typeof street !== "undefined") {
        conditions = { ...conditions, street };
      }

      if (typeof pincode !== "undefined") {
        conditions = { ...conditions, pincode };
      }

      if (typeof isPublished !== "undefined") {
        conditions = { ...conditions, isPublished };
      }

      let query = TempleModel.find(conditions);

      query.limit(limit);

      // NOTE: each page will return 10 temples, so we need to skip
      // 10 temples per page
      query.skip((page - 1) * limit);

      // NOTE: 1 => increasing order
      // NOTE: -1 => decreasing order
      query.sort({
        name: 1,
        // createdAt: "desc",
      });

      return query;
    },
  },
  Mutation: {
    searchTemples: async (parent, args, context, info) => {
      const { query, nextToken } = args.data;

      const baseURI = "https://maps.googleapis.com/maps/api/place";
      const requestType = "/textsearch";
      // const requestType = "findplacefromtext";

      const outputFormat = "/json";
      const key = `key=${GOOGLE_MAP_API_KEY}`;
      const input = `input=${query}`;
      const inputType = `inputtype=textquery`;
      const fields = `fields=name,geometry,formatted_address,place_id,photos`;
      const type = `type=hindu_temple`;

      let URL = `${baseURI}${requestType}${outputFormat}?${input}&${inputType}&${fields}&${key}&${type}`;

      if (typeof nextToken !== "undefined") {
        const nextPageToken = `next_page_token=${nextToken}`;

        URL += nextPageToken;
      }

      const res = await fetch(URL);

      return res.json();
    },
    importTemple: async (parent, args, context, info) => {
      const { placeId } = args.data;

      const baseURI = "https://maps.googleapis.com/maps/api/place";
      const requestType = "/details";

      const outputFormat = "/json";
      const key = `key=${GOOGLE_MAP_API_KEY}`;
      const input = `place_id=${placeId}`;
      // const fields = `fields=address_component,adr_address,business_status,formatted_address,geometry,icon,name,permanently_closed,photo,place_id,plus_code,type,url,utc_offset,vicinity`;
      const fields = `fields=address_component,geometry,name,place_id`;

      let URL = `${baseURI}${requestType}${outputFormat}?${input}&${fields}&${key}`;

      const res = await fetch(URL);

      const data = await res.json();

      const { address_components, geometry, name } = data.result;
      const { lng: longitude, lat: latitude } = geometry.location;
      let street = "";
      let district = "";
      let state = "";
      let country = "";
      let pincode = "";
      let coordinates = [longitude, latitude];
      let neighborhood = "";
      let sublocality_level_2 = "";
      let sublocality_level_1 = "";
      let locality = "";

      address_components.map(({ long_name: name, types }) => {
        if (types.findIndex((v) => v === "country") >= 0) {
          country = name;
        } else if (
          types.findIndex((v) => v === "administrative_area_level_1") >= 0
        ) {
          state = name;
        } else if (
          types.findIndex((v) => v === "administrative_area_level_2") >= 0
        ) {
          district = name;
        } else if (types.findIndex((v) => v === "postal_code") >= 0) {
          pincode = name;
        } else if (types.findIndex((v) => v === "neighborhood") >= 0) {
          neighborhood = name;
        } else if (types.findIndex((v) => v === "sublocality_level_2") >= 0) {
          sublocality_level_2 = name;
        } else if (types.findIndex((v) => v === "sublocality_level_1") >= 0) {
          sublocality_level_1 = name;
        } else if (types.findIndex((v) => v === "locality") >= 0) {
          locality = name;
        }
      });

      if (neighborhood.length > 0) {
        street += `${neighborhood}`;
      }

      if (sublocality_level_2.length > 0) {
        street += `, ${sublocality_level_2}`;
      }

      if (sublocality_level_1.length > 0) {
        street += `, ${sublocality_level_1}`;
      }

      if (locality.length > 0) {
        street += `, ${locality}`;
      }

      const newTemple = new TempleModel({
        name,
        placeId,

        location: {
          coordinates,
        },

        street,
        district,
        state,
        country,
        pincode,
      });

      return newTemple.save();
    },
    createTemple: async (parent, args, context, info) => {
      const { name } = args.data;

      const newTemple = new TempleModel({
        name,
      });

      return newTemple.save();
    },
    updateTemple: async (parent, args, context, info) => {
      const {
        id,
        name,
        description,
        profilePic,
        coverPic,

        country,
        state,
        district,
        street,
        pincode,
        longitude,
        latitude,

        isPublished,
      } = args.data;

      let data = {};

      if (typeof name !== "undefined") {
        data = { ...data, name };
      }
      if (typeof description !== "undefined") {
        data = { ...data, description };
      }
      if (typeof profilePic !== "undefined") {
        data = { ...data, profilePic };
      }
      if (typeof coverPic !== "undefined") {
        data = { ...data, coverPic };
      }
      if (typeof country !== "undefined") {
        data = { ...data, country };
      }
      if (typeof state !== "undefined") {
        data = { ...data, state };
      }
      if (typeof district !== "undefined") {
        data = { ...data, district };
      }
      if (typeof street !== "undefined") {
        data = { ...data, street };
      }
      if (typeof pincode !== "undefined") {
        data = { ...data, pincode };
      }
      if (typeof isPublished !== "undefined") {
        data = { ...data, isPublished };
      }

      if (typeof longitude !== "undefined" && typeof latitude !== "undefined") {
        data = {
          ...data,
          location: {
            coordinates: [longitude, latitude],
          },
        };
      }

      const options = { new: true };

      console.log(data);

      const res = await TempleModel.findByIdAndUpdate(
        id,
        data,
        options,
      ).catch((err) => console.log(err));

      console.log(res);

      return res;
    },
    deleteTemple: async (parent, args, context, info) => {
      const { id } = args.data;

      return TempleModel.findOneAndRemove(id);
    },
  },
};

module.exports = resolvers;
