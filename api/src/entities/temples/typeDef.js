/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type Temple {
    id: ID!
    name: String
    description: String
    profilePic: String
    coverPic: String

    location: Location
    # address: Address
    country: String
    state: String
    district: String
    street: String
    pincode: String

    placeId: String

    isPublished: Boolean
  }

  input TempleWhereInput {
    id: ID!
  }

  input TempleImportInput {
    placeId: String!
  }

  input TempleSearchInput {
    query: String!
    nextToken: String
  }

  input TempleCountWhereInput {
    isPublished: Boolean
  }

  input TemplesWhereInput {
    id: ID
    ids: [ID]
    placeId: String
    page: Int
    limit: Int
    country: String
    state: String
    district: String
    street: String
    pincode: String

    isPublished: Boolean
  }

  input TempleCreateInput {
    name: String!
  }

  input TempleUpdateInput {
    id: ID!
    name: String
    description: String
    profilePic: String
    coverPic: String

    longitude: Float
    latitude: Float
    country: String
    state: String
    district: String
    street: String
    pincode: String

    isPublished: Boolean
  }

  input TempleDeleteInput {
    id: ID!
  }

  extend type Query {
    temple(where: TempleWhereInput!): Temple
    templeCount(where: TempleCountWhereInput!): Int
    temples(where: TemplesWhereInput!): [Temple]
  }

  extend type Mutation {
    createTemple(data: TempleCreateInput!): Temple!
    updateTemple(data: TempleUpdateInput!): Temple!
    deleteTemple(data: TempleDeleteInput!): Boolean

    searchTemples(data: TempleSearchInput!): JSON
    importTemple(data: TempleImportInput!): Temple!
  }
`;

module.exports = typeDef;
