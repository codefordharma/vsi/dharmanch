/** @format */

const mongoose = require("../../database/init");

const { AddressSchema } = require("../common/model");

const Schema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      maxlength: 300,
    },
    description: {
      type: String,
      default: "",
    },
    profilePic: {
      type: String,
    },
    coverPic: {
      type: String,
    },
    location: {
      type: {
        type: String,
        enum: ["Point"],
        required: true,
        default: "Point",
      },
      coordinates: {
        type: [Number],
        required: true,
        default: [0, 0],
      },
    },
    // address: {
    //   type: AddressSchema,
    // },
    country: {
      type: String,
      default: "",
    },
    state: {
      type: String,
      default: "",
    },
    district: {
      type: String,
      default: "",
    },
    street: {
      type: String,
      default: "",
    },
    pincode: {
      type: String,
      default: "",
    },

    placeId: {
      type: String,
      required: true,
      unique: true,
    },

    isPublished: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  },
);

Schema.index({
  location: "2dsphere",
});

const Model = mongoose.model("temple", Schema, "temples");

module.exports = Model;
