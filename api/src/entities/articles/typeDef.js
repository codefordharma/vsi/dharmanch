/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type BlockSchema {
    type: String
    data: JSON
  }

  type ArticleContent {
    version: String!
    time: Date!
    blocks: [BlockSchema]!
  }

  input BlockSchemaInput {
    type: String
    data: JSON
  }

  input ArticleContentInput {
    version: String!
    time: Date!
    blocks: [BlockSchemaInput]!
  }

  type Article {
    id: ID!

    title: String
    description: String
    content: ArticleContent

    isPublished: Boolean

    createdAt: Date
    updatedAt: Date
  }

  input ArticleWhereInput {
    id: ID!
  }

  input ArticlesWhereInput {
    id: ID
    ids: [ID]
    templeId: ID
    deityId: ID

    isPublished: Boolean

    limit: Int
    page: Int
  }

  input ArticleCreateInput {
    templeId: ID!
    title: String!
  }

  input ArticleUpdateInput {
    id: ID!

    title: String
    description: String
    content: ArticleContentInput

    isPublished: Boolean
  }

  input ArticleDeleteInput {
    id: ID!
  }

  extend type Query {
    blog(where: ArticleWhereInput!): Article
    blogs(where: ArticlesWhereInput!): [Article]
  }

  extend type Mutation {
    createArticle(data: ArticleCreateInput!): Article
    updateArticle(data: ArticleUpdateInput!): Article
    deleteArticle(data: ArticleDeleteInput!): Boolean
  }
`;

module.exports = typeDef;
