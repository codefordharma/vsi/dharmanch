/** @format */

const { ObjectId } = require("mongoose").Types;

const ArticleModel = require("./model");

const resolvers = {
  Query: {
    article: async (parent, args, context, info) => {
      const { id } = args.where;

      return ArticleModel.findById(id);
    },
    articles: async (parent, args, context, info) => {
      const {
        id,
        ids,
        templeId,
        deityId,
        page = 1,
        limit = 10,
        isPublished,
      } = args.where;

      if (page < 1) {
        throw new Error(`Page count can't be less then 1.`);
      }

      let conditions = {};

      if (typeof id !== "undefined") {
        conditions = { ...conditions, _id: id };
      }

      if (typeof ids !== "undefined") {
        conditions = { ...conditions, _id: { $in: ids } };
      }

      if (typeof templeId !== "undefined") {
        conditions = { ...conditions, templeId };
      }

      if (typeof deityId !== "undefined") {
        conditions = { ...conditions, deityId };
      }

      if (typeof isPublished !== "undefined") {
        conditions = { ...conditions, isPublished };
      }

      const query = ArticleModel.find(conditions);

      query.limit(limit);

      query.skip((page - 1) * limit);

      // NOTE: 1 => increasing order
      // NOTE: -1 => decreasing order
      query.sort({
        // title: 1,
        createdAt: "desc",
      });

      return query;
    },
  },
  Mutation: {
    createArticle: async (parent, args, context, info) => {
      const { title, templeId } = args.data;

      const newArticle = new ArticleModel({
        templeId,
        title,
      });

      return newArticle.save();
    },
    updateArticle: async (parent, args, context, info) => {
      const { id, title, description, content, isPublished } = args.data;

      let data = {};

      if (typeof title !== "undefined") {
        data = { ...data, title };
      }

      if (typeof description !== "undefined") {
        data = { ...data, description };
      }

      // TODO: optimise this content part
      if (typeof content !== "undefined") {
        data = { ...data, content };
      }

      if (typeof isPublished !== "undefined") {
        data = { ...data, isPublished };
      }

      const options = { new: true };

      return ArticleModel.findByIdAndUpdate(id, data, options);
    },
    deleteArticle: async (parent, args, context, info) => {
      const { id } = args.data;

      return ArticleModel.findOneAndRemove(id);
    },
  },
};

module.exports = resolvers;
