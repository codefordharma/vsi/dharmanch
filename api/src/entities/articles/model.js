/** @format */

const mongoose = require("../../database/init");

const { ObjectId } = mongoose.Types;

const BlockSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  data: {
    type: JSON,
  },
});

const ArticleContentSchema = new mongoose.Schema({
  version: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    required: true,
  },
  blocks: {
    type: [BlockSchema],
    required: true,
  },
});

const ArticleSchema = new mongoose.Schema(
  {
    templeId: {
      type: ObjectId,
    },
    deityId: {
      type: ObjectId,
    },
    title: {
      type: String,
      required: true,
      maxlength: 150,
    },
    description: {
      type: String,
      maxlength: 500,
    },
    keywords: {
      type: [String],
    },
    content: {
      type: ArticleContentSchema,
    },
    isPublished: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  },
);

const Model = mongoose.model("article", ArticleSchema, "articles");

module.exports = Model;
