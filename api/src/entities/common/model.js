/** @format */

const mongoose = require("../../database/init");

const AddressSchema = new mongoose.Schema({
  country: {
    type: String,
  },
  state: {
    type: String,
  },
  district: {
    type: String,
  },
  street: {
    type: String,
  },
  pinCode: {
    type: String,
  },
});

module.exports = {
  AddressSchema,
};
