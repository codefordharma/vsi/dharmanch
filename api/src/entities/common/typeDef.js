/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  scalar Date
  scalar JSON

  enum PERMISSION {
    ADMIN
    USER
  }

  type Location {
    type: String!
    coordinates: [Float]
  }

  type Address {
    country: String
    state: String
    district: String
    street: String
    pinCode: String
  }

  # extend type Query { }

  # extend type Mutation { }
`;

module.exports = typeDef;
