/** @format */

const mongoose = require("../../database/init");
const { ObjectId } = mongoose.Types;

const Schema = new mongoose.Schema(
  {
    templeId: {
      type: ObjectId,
    },
    deityId: {
      type: ObjectId,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    url: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  },
);

Schema.index({
  url: 1,
});

const Model = mongoose.model("file", Schema, "files");

module.exports = Model;
