/** @format */

const { GOOGLE_CLOUD_BUCKET_NAME } = process.env;

const mongoose = require("mongoose");
const { Storage } = require("@google-cloud/storage");

const FileModel = require("./model");

// Creates a client
const storage = new Storage();
const bucket = storage.bucket(GOOGLE_CLOUD_BUCKET_NAME);

async function UpdateFile(req, res) {
  const { originalname, mimetype, buffer } = req.files[0];
  const { templeId, deityId } = req.body;

  const originalnameArray = originalname.split(".");
  const fileExtention = originalnameArray[originalnameArray.length - 1];
  const options = {
    gzip: true,
    resumable: false,
    metadata: {
      contentType: mimetype,
      cacheControl: "public, max-age=31536000",
    },
  };

  // fileId
  const fileId = new mongoose.Types.ObjectId();

  const fileName = `files/${fileId}.${fileExtention}`;

  const file = bucket.file(fileName);

  file
    .createWriteStream(options)
    .on("finish", async () => {
      const url = `https://storage.googleapis.com/${bucket.name}/${file.name}`;

      file.makePublic().then().catch();

      const newFile = new FileModel({
        _id: fileId,
        url,
        templeId,
        deityId,
      });

      try {
        const file2 = await newFile.save();
        res.json(file2.toJSON());
      } catch (error) {
        res.json({ error: "error" });
      }
    })
    .on("error", () => {
      res.json({ error: "error" });
    })
    .end(buffer);
}

module.exports = UpdateFile;
