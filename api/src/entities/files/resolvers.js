/** @format */

// const { ObjectId } = require("mongoose").Types;

const FileModel = require("./model");

const resolvers = {
  Query: {
    file: async (parent, args, context, info) => {
      const { id } = args.where;

      return FileModel.findById(id);
    },
    files: async (parent, args, context, info) => {
      // NOTE: we will give result step wise, with only 10 files at
      // a time & as the number of 'page' increases, will give
      // next 10 files
      const { id, ids, limit = 10, page = 0 } = args.where;

      let conditions = {};

      if (typeof id !== "undefined") {
        conditions = {
          ...conditions,
          _id: id,
        };
      }

      if (typeof ids !== "undefined") {
        conditions = {
          ...conditions,
          _id: {
            $in: ids,
          },
        };
      }

      let query = FileModel.find(conditions);

      query.limit(limit);

      // NOTE: each page will return 10 files, so we need to skip
      // 10 files per page
      query.skip(page * limit);

      query.sort({ createdAt: "desc" });

      return query;
    },
  },
  // Mutation: {
  //   createTemple: async (parent, args, context, info) => {
  //     const { name } = args.data;

  //     const newTemple = new FileModel({
  //       name,
  //     });

  //     return newTemple.save();
  //   },
  //   updateTemple: async (parent, args, context, info) => {
  //     const { id, name, description, longitude, latitude } = args.data;

  //     let data = {};

  //     if (typeof name !== "undefined") {
  //       data = {
  //         ...data,
  //         name,
  //       };
  //     }

  //     if (typeof description !== "undefined") {
  //       data = {
  //         ...data,
  //         description,
  //       };
  //     }

  //     const options = { new: true };

  //     return FileModel.findByIdAndUpdate(id, data, options);
  //   },
  //   deleteTemple: async (parent, args, context, info) => {
  //     const { id } = args.data;

  //     return FileModel.findOneAndRemove(id);
  //   },
  // },
};

module.exports = resolvers;
