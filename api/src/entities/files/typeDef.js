/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type File {
    id: ID!
    title: String!
    description: String
  }

  input FileWhereInput {
    id: ID!
  }

  input FilesWhereInput {
    id: ID
    ids: [ID]
    page: Int
    limit: Int
  }

  input FileCreateInput {
    title: String!
  }

  input FileUpdateInput {
    title: String
    description: String
  }

  extend type Query {
    file(where: FileWhereInput!): File
    files(where: FilesWhereInput!): [File]
  }

  # extend type Mutation {
  #   createFile(data: FileCreateInput!): File!
  #   updateFile(data: FileUpdateInput!): File!
  #   deleteFile: Boolean
  # }
`;

module.exports = typeDef;
