/** @format */
const { gql } = require("apollo-server");

const common = require("./common");

const blogs = require("./blogs");
const deities = require("./deities");
const files = require("./files");
const photos = require("./photos");
const temples = require("./temples");
const users = require("./users");
const videos = require("./videos");
const search = require("./search");

const resolvers = [
  blogs.resolvers,
  common.resolvers,
  deities.resolvers,
  files.resolvers,
  photos.resolvers,
  temples.resolvers,
  users.resolvers,
  videos.resolvers,
  search.resolvers,
];

const typeDef = gql`
  type Query
  type Mutation
`;

const typeDefs = [
  typeDef,
  common.typeDef,

  blogs.typeDef,
  deities.typeDef,
  files.typeDef,
  photos.typeDef,
  temples.typeDef,
  users.typeDef,
  videos.typeDef,
  search.typeDef,
];

module.exports = {
  resolvers,
  typeDefs,
};
