/** @format */

const mongoose = require("../../database/init");

const { ObjectId } = mongoose.Types;

// NOTE: either templeId or deityId is required
const Schema = new mongoose.Schema(
  {
    templeId: {
      type: ObjectId,
    },
    deityId: {
      type: ObjectId,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    url: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  },
);

Schema.index({
  url: 1,
});

const Model = mongoose.model("photo", Schema, "photos");

module.exports = Model;
