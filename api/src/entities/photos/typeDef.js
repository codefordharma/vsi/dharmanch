/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type Photo {
    id: ID!
    title: String
    description: String
    url: String!
    templeId: ID

    createdAt: Date
    updatedAt: Date
  }

  input PhotoWhereInput {
    id: ID!
  }

  input PhotoCountWhereInput {
    templeId: ID
  }

  input PhotosWhereInput {
    id: ID
    templeId: ID
    ids: [ID]
    page: Int
    limit: Int
  }

  input PhotoCreateInput {
    title: String!
  }

  input PhotoUpdateInput {
    title: String
    description: String
  }

  extend type Query {
    photo(where: PhotoWhereInput!): Photo
    photoCount(where: PhotoCountWhereInput!): Int!
    photos(where: PhotosWhereInput!): [Photo]
  }

  # extend type Mutation {
  # createPhoto(data: PhotoCreateInput!): Photo!
  # updatePhoto(data: PhotoUpdateInput!): Photo!
  # deletePhoto: Boolean
  # }
`;

module.exports = typeDef;
