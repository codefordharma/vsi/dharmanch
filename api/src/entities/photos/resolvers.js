/** @format */

// const { ObjectId } = require("mongoose").Types;

const PhotoModel = require("./model");

const resolvers = {
  Query: {
    photo: async (parent, args, context, info) => {
      const { id } = args.where;

      return PhotoModel.findById(id);
    },
    photoCount: async (parent, args, context, info) => {
      const { templeId } = args.where;
      let conditions = {};

      if (typeof templeId !== "undefined") {
        conditions = { ...conditions, templeId };
      }

      return PhotoModel.countDocuments(conditions);
    },
    photos: async (parent, args, context, info) => {
      // NOTE: we will give result step wise, with only 10 photos at
      // a time & as the number of 'page' increases, will give
      // next 10 photos
      const { id, templeId, ids, limit = 10, page = 1 } = args.where;

      if (page < 1) {
        throw new Error(`Page number can't be less then 1.`);
      }

      let conditions = {};

      if (typeof id !== "undefined") {
        conditions = {
          ...conditions,
          _id: id,
        };
      }

      if (typeof ids !== "undefined") {
        conditions = {
          ...conditions,
          _id: {
            $in: ids,
          },
        };
      }

      if (typeof templeId !== "undefined") {
        conditions = {
          ...conditions,
          templeId,
        };
      }

      const query = PhotoModel.find(conditions);

      query.limit(limit);

      // NOTE: each page will return 10 photos, so we need to skip
      // 10 photos per page
      query.skip((page - 1) * limit);

      query.sort({ createdAt: "desc" });

      return query;
    },
  },
  // Mutation: {
  // createPhoto: async (parent, args, context, info) => {
  //   const { url } = args.data;
  //   const newPhoto = new PhotoModel({
  //     url,
  //   });
  //   return newPhoto.save();
  // },
  // updatePhoto: async (parent, args, context, info) => {
  //   const { id, title, description, url } = args.data;
  //   let data = {};
  //   if (typeof title !== "undefined") {
  //     data = {
  //       ...data,
  //       title,
  //     };
  //   }
  //   if (typeof description !== "undefined") {
  //     data = {
  //       ...data,
  //       description,
  //     };
  //   }
  //   if (typeof url !== "undefined") {
  //     data = {
  //       ...data,
  //       url,
  //     };
  //   }
  //   const options = { new: true };
  //   return PhotoModel.findByIdAndUpdate(id, data, options);
  // },
  // deletePhoto: async (parent, args, context, info) => {
  //   const { id } = args.data;
  //   return PhotoModel.findOneAndRemove(id);
  // },
  // },
};

module.exports = resolvers;
