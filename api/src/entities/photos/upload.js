/** @format */

const { NODE_ENV, GOOGLE_CLOUD_BUCKET_NAME } = process.env;

const mongoose = require("mongoose");
const { Storage } = require("@google-cloud/storage");

const PhotoModel = require("./model");

// Creates a client
const storage = new Storage();
const bucket = storage.bucket(GOOGLE_CLOUD_BUCKET_NAME);

const isDev = NODE_ENV !== "development" || NODE_ENV !== "production";

async function UpdatePhoto(req, res) {
  const { originalname, mimetype, buffer } = req.files[0];
  const { templeId, deityId } = req.body;

  const originalnameArray = originalname.split(".");
  const fileExtention = originalnameArray[originalnameArray.length - 1];
  const options = {
    gzip: true,
    resumable: false,
    metadata: {
      contentType: mimetype,
      cacheControl: "public, max-age=31536000",
    },
  };

  // photoId
  const photoId = new mongoose.Types.ObjectId();

  const fileName = `${isDev ? "dev/" : ""}photos/${photoId}.${fileExtention}`;

  const file = bucket.file(fileName);

  file
    .createWriteStream(options)
    .on("finish", async () => {
      const url = `https://storage.googleapis.com/${bucket.name}/${file.name}`;

      file.makePublic().then().catch();

      const newPhoto = new PhotoModel({
        _id: photoId,
        url,
        templeId,
        deityId,
      });

      try {
        const photo = await newPhoto.save();
        const pho = photo.toJSON();
        // TODO: json response is not loading in browser.
        res.status(200).json(pho);
      } catch (error) {
        res.status(500).json({ error: "message" });
      }
    })
    .on("error", () => {
      res.status(500).json({ error: "message" });
    })
    .end(buffer);
}

module.exports = UpdatePhoto;
