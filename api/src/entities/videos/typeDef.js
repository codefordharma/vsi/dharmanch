/** @format */

const { gql } = require("apollo-server");

const typeDef = gql`
  type Video {
    id: ID!
    title: String!
    description: String
  }

  input VideoWhereInput {
    id: ID!
  }

  input VideosWhereInput {
    id: ID
    ids: [ID]
    page: Int
    limit: Int
  }

  input VideoCreateInput {
    title: String!
  }

  input VideoUpdateInput {
    title: String
    description: String
  }

  extend type Query {
    video(where: VideoWhereInput!): Video
    videos(where: VideosWhereInput!): [Video]
  }

  # extend type Mutation {
  #   createVideo(data: VideoCreateInput!): Video!
  #   updateVideo(data: VideoUpdateInput!): Video!
  #   deleteVideo: Boolean
  # }
`;

module.exports = typeDef;
