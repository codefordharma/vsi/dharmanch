/** @format */

// const { ObjectId } = require("mongoose").Types;

const VideoModel = require("./model");

const resolvers = {
  Query: {
    video: async (parent, args, context, info) => {
      const { id } = args.where;

      return VideoModel.findById(id);
    },
    videos: async (parent, args, context, info) => {
      // NOTE: we will give result step wise, with only 10 videos at
      // a time & as the number of 'page' increases, will give
      // next 10 videos
      const { id, ids, limit = 10, page = 0 } = args.where;

      let conditions = {};

      if (typeof id !== "undefined") {
        conditions = {
          ...conditions,
          _id: id,
        };
      }

      if (typeof ids !== "undefined") {
        conditions = {
          ...conditions,
          _id: {
            $in: ids,
          },
        };
      }

      let query = VideoModel.find(conditions);

      query.limit(limit);

      // NOTE: each page will return 10 videos, so we need to skip
      // 10 videos per page
      query.skip(page * limit);

      query.sort({ createdAt: "desc" });

      return query;
    },
  },
  // Mutation: {
  //   createTemple: async (parent, args, context, info) => {
  //     const { name } = args.data;

  //     const newTemple = new VideoModel({
  //       name,
  //     });

  //     return newTemple.save();
  //   },
  //   updateTemple: async (parent, args, context, info) => {
  //     const { id, name, description, longitude, latitude } = args.data;

  //     let data = {};

  //     if (typeof name !== "undefined") {
  //       data = {
  //         ...data,
  //         name,
  //       };
  //     }

  //     if (typeof description !== "undefined") {
  //       data = {
  //         ...data,
  //         description,
  //       };
  //     }

  //     if (typeof longitude !== "undefined" && typeof latitude !== "undefined") {
  //       data = {
  //         ...data,
  //         location: {
  //           coordinates: [longitude, latitude],
  //         },
  //       };
  //     }

  //     const options = { new: true };

  //     return VideoModel.findByIdAndUpdate(id, data, options);
  //   },
  //   deleteTemple: async (parent, args, context, info) => {
  //     const { id } = args.data;

  //     return VideoModel.findOneAndRemove(id);
  //   },
  // },
};

module.exports = resolvers;
