/** @format */

const { GOOGLE_CLOUD_BUCKET_NAME } = process.env;

const mongoose = require("mongoose");
const { Storage } = require("@google-cloud/storage");

const VideoModel = require("./model");

// Creates a client
const storage = new Storage();
const bucket = storage.bucket(GOOGLE_CLOUD_BUCKET_NAME);

async function UpdateVideo(req, res) {
  const { originalname, mimetype, buffer } = req.files[0];
  const { templeId, deityId } = req.body;

  const originalnameArray = originalname.split(".");
  const fileExtention = originalnameArray[originalnameArray.length - 1];
  const options = {
    gzip: true,
    resumable: false,
    metadata: {
      contentType: mimetype,
      cacheControl: "public, max-age=31536000",
    },
  };

  // videoId
  const videoId = new mongoose.Types.ObjectId();

  const fileName = `videos/${videoId}.${fileExtention}`;

  const file = bucket.file(fileName);

  file
    .createWriteStream(options)
    .on("finish", async () => {
      const url = `https://storage.googleapis.com/${bucket.name}/${file.name}`;

      file.makePublic().then().catch();

      const newVideo = new VideoModel({
        _id: videoId,
        url,
        templeId,
        deityId,
      });

      try {
        const video = await newVideo.save();
        res.json(video.toJSON());
      } catch (error) {
        res.json({ error: "error" });
      }
    })
    .on("error", () => {
      res.json({ error: "error" });
    })
    .end(buffer);
}

module.exports = UpdateVideo;
