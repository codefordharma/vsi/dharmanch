/** @format */

const assert = require("assert");

let { NODE_ENV } = process.env;

if (NODE_ENV !== "development" && NODE_ENV !== "production") {
  // eslint-disable-next-line global-require
  require("dotenv").config({ path: ".env" });
}

NODE_ENV = process.env.NODE_ENV;

const {
  AUTH_SECRET,
  GOOGLE_CLOUD_BUCKET_NAME,
  GOOGLE_MAP_API_KEY,
  MONGODB_URI,
} = process.env;

assert(NODE_ENV, "NODE_ENV env variable is required");
assert(MONGODB_URI, "MONGO DB URI env variable is required");
assert(AUTH_SECRET, "AUTH_SECRET env variable is required");
assert(GOOGLE_MAP_API_KEY, "GOOGLE_MAP_API_KEY env variable is required");
assert(
  GOOGLE_CLOUD_BUCKET_NAME,
  "GOOGLE_CLOUD_BUCKET_NAME env variable is required",
);
