/** @format */

// NOTE: this file contains all the configs related to this application.

const { CORS_URL } = process.env;

const corsOptions = {
  credentials: true,
  origin: CORS_URL.split(","),
};

module.exports = {
  corsOptions,
};
