/** @format */

module.exports = {
  extends: ["airbnb", "prettier"],
  plugins: ["prettier"],
  env: {
    jest: true,
  },
  rules: {
    "prettier/prettier": ["error"],

    "no-console": "warn",
    "no-underscore-dangle": "warn",
  },
};
